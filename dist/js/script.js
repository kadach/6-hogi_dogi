(function ($) {


    //HEADER NAV MENU *************************************************************
    var navChild = $('.nav-child a');
    var navChildMenu = $('.nav-menu-child');
    var navChildMenuLi = $('.nav-menu-child li');
    var navChildMenuHeight = navChildMenuLi.height();
    navChild.hover(function (e) {
        if (navChild.hasClass('open')) {
            navChildMenu.css('height', '0px');
            navChild.removeClass('open');
        } else {
            navChildMenu.css('height', navChildMenuHeight * navChildMenuLi.length + 'px');
            navChild.addClass('open');
        }
    });

    //MOBILE TELL
    var mobBtn = $('.mobile-tel-btn > span');
    var mobTell = $('.mobile-tell');
    var closeTell = $('.close-tell');
    mobBtn.click(function () {
        mobTell.addClass('life');
        setTimeout(function () {
            mobTell.addClass('open');
        },30)
    });
    closeTell.click(function () {
        mobTell.removeClass('open');
        setTimeout(function () {
            mobTell.removeClass('life');
        },530)
    });

    var btnPopUp = $('.btn-pop-up');
    var popUp = $('.pop-up');
    btnPopUp.click(function () {
        popUp.addClass('life');
        setTimeout(function () {
            popUp.addClass('see');
        },30);
        popUp.click(function () {
            if($(event.target).hasClass('pop-up')){
                popUp.removeClass('see');
                setTimeout(function () {
                    popUp.removeClass('life');
                },550)
            }
        });
    });

    var check = $('.check-wrap label');
    check.click(function () {
        check.removeClass('active');
        $(this).addClass('active');
    });

    var scrollGo = $('[data-scroll]');
    scrollGo.click(function () {
        $('body').animate({
            scrollTop: $('#form').offset().top}, 1000);
    });

    var oneCashBtn = $('.one-cash');
    var noCash = $('.non-cash');
    oneCashBtn.click(function () {
        oneCashBtn.removeClass('active');
        $(this).addClass('active');
        if($(this).hasClass('open')){
            noCash.addClass('active');
        } else {
            noCash.removeClass('active');
        }
    });







})(jQuery);